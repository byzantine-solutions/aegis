const { expect } = require("chai");


const getAmountFromFullTokens = (number) => {
  if (number < 1) {
    return hre.ethers.constants.WeiPerEther.mul(number * 10000).div(10000)
  } else {
    return hre.ethers.constants.WeiPerEther.mul(number)
  }

}

const advanceTime = async (days) => {
    await hre.network.provider.request({
        method: "evm_increaseTime",
        params: [days * 3600 * 24]
    });
    await hre.network.provider.request({
        method: "evm_mine",
        params: []
    });
};

const advanceTimeSeconds = async (seconds) => {
    await hre.network.provider.request({
        method: "evm_increaseTime",
        params: [seconds]
    });
    await hre.network.provider.request({
        method: "evm_mine",
        params: []
    });
};

const getChainTimestamp = async () => {
  return (await hre.ethers.provider.getBlock()).timestamp
}

describe("NoteDeployment", function() {
  beforeEach(async () => {
        [deployer, issuer1, issuer2, buyer1, buyer2] = await hre.ethers.getSigners();

        let noteDeployerFactory = await hre.ethers.getContractFactory("NoteDeployer");
        noteDeployer = await noteDeployerFactory.deploy()

        let auctionDeployerFactory = await hre.ethers.getContractFactory("AuctionDeployer");
        auctionDeployer = await auctionDeployerFactory.deploy()

        let erc20Factory = await hre.ethers.getContractFactory("ERC20Mock");

        aegisToken = await erc20Factory.deploy("Aegis Token", "AEGIS");
        daiToken = await erc20Factory.deploy("Dai", "DAI");
        lpsToken = await erc20Factory.deploy("Pool LPS", "LPS");

        let athenaFactory = await hre.ethers.getContractFactory("AthenaMock");
        athena = await athenaFactory.deploy(aegisToken.address);

        await aegisToken.mint(deployer.address, getAmountFromFullTokens(50));
        await daiToken.mint(issuer1.address, getAmountFromFullTokens(1000));
        await daiToken.mint(issuer2.address, getAmountFromFullTokens(1000));
        await daiToken.mint(buyer1.address, getAmountFromFullTokens(1000));
        await daiToken.mint(buyer2.address, getAmountFromFullTokens(1000));

        let factoryFactory = await hre.ethers.getContractFactory("NoteFactory");
        factory = await factoryFactory.deploy(aegisToken.address, athena.address, noteDeployer.address, auctionDeployer.address);


    });

    it('should deploy note and auction contracts with expected parameters', async function () {
      let factoryDeployerConnection = factory.connect(deployer)
      let aegisDeployerConnection = aegisToken.connect(deployer)

      let noteCreationBond = await athena.getPCNCreationBond()
      await aegisDeployerConnection.approve(factory.address, noteCreationBond)

      let currentTimestamp = await getChainTimestamp()

      let expiryTime = currentTimestamp + 60*60*24*30

      addresses = await factoryDeployerConnection.callStatic.deployNote(daiToken.address, lpsToken.address, 900, 2500, expiryTime)
      await factoryDeployerConnection.deployNote(daiToken.address, lpsToken.address, 900, 2500, expiryTime)

      currentTimestamp = await getChainTimestamp()
      let auctionStart = currentTimestamp + 60*60*12
      let auctionEnd = currentTimestamp + 60*60*24

      let noteFactory = await hre.ethers.getContractFactory("CoverageNote")
      note = noteFactory.attach(addresses.noteAddress)

      let auctionFactory = await hre.ethers.getContractFactory("GenesisAuction")
      auction = auctionFactory.attach(addresses.auctionAddress)

      expect(await note.collateralToken()).to.equal(daiToken.address)
      expect(await note.coveredPoolLPS()).to.equal(lpsToken.address)
      expect(await note.coverageFactor()).to.equal(900)
      expect(await note.maxLeverage()).to.equal(2500)
      expect(await note.expiryTimestamp()).to.equal(expiryTime)

      expect(await auction.startTimestamp()).to.equal(auctionStart)
      expect(await auction.endTimestamp()).to.equal(auctionEnd)
      expect(await auction.premiumToken()).to.equal(daiToken.address)
      expect(await auction.noteToken()).to.equal(note.address)
      expect(await auction.getCurrentPrice()).to.equal(getAmountFromFullTokens(0.9))
      expect(await auction.getStage()).to.equal(0)

      let headAskId = await auction.getAskId(ethers.constants.AddressZero, 0, 0)
      let tailAskId = await auction.getAskId(ethers.constants.AddressZero, 0, getAmountFromFullTokens(0.9))

      let askList = await auction.getAskList()

      expect(headAskId).to.equal(askList[0])
      expect(tailAskId).to.equal(askList[1])
    });
});

describe("NoteIssuance", function() {
  beforeEach(async () => {
        [deployer, issuer1, issuer2, buyer1, buyer2] = await hre.ethers.getSigners();

        let noteDeployerFactory = await hre.ethers.getContractFactory("NoteDeployer");
        noteDeployer = await noteDeployerFactory.deploy()

        let auctionDeployerFactory = await hre.ethers.getContractFactory("AuctionDeployer");
        auctionDeployer = await auctionDeployerFactory.deploy()

        let erc20Factory = await hre.ethers.getContractFactory("ERC20Mock");

        aegisToken = await erc20Factory.deploy("Aegis Token", "AEGIS");
        daiToken = await erc20Factory.deploy("Dai", "DAI");
        lpsToken = await erc20Factory.deploy("Pool LPS", "LPS");

        let athenaFactory = await hre.ethers.getContractFactory("AthenaMock");
        athena = await athenaFactory.deploy(aegisToken.address);

        await aegisToken.mint(deployer.address, getAmountFromFullTokens(50));
        await daiToken.mint(issuer1.address, getAmountFromFullTokens(1000));
        await daiToken.mint(issuer2.address, getAmountFromFullTokens(1000));
        await daiToken.mint(buyer1.address, getAmountFromFullTokens(1000));
        await daiToken.mint(buyer2.address, getAmountFromFullTokens(1000));

        let factoryFactory = await hre.ethers.getContractFactory("NoteFactory");
        factory = await factoryFactory.deploy(aegisToken.address, athena.address, noteDeployer.address, auctionDeployer.address);

        let factoryDeployerConnection = factory.connect(deployer)
        let aegisDeployerConnection = aegisToken.connect(deployer)

        let noteCreationBond = await athena.getPCNCreationBond()
        await aegisDeployerConnection.approve(factory.address, noteCreationBond)

        let expiryTime = await getChainTimestamp() + 60*60*24*30

        addresses = await factoryDeployerConnection.callStatic.deployNote(daiToken.address, lpsToken.address, 900, 2500, expiryTime)
        await factoryDeployerConnection.deployNote(daiToken.address, lpsToken.address, 900, 2500, expiryTime)

        let noteFactory = await hre.ethers.getContractFactory("CoverageNote")
        note = noteFactory.attach(addresses.noteAddress)

    });

    it('should issue and unwind note tokens according to the coverage factor', async function () {
      let noteIssuerConnection = note.connect(issuer1)
      let daiIssuerConnection = daiToken.connect(issuer1)

      await daiIssuerConnection.approve(note.address, getAmountFromFullTokens(90))
      await noteIssuerConnection.issue(getAmountFromFullTokens(100), issuer1.address)

      expect(await daiIssuerConnection.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(910))
      expect(await daiIssuerConnection.balanceOf(note.address)).to.equal(getAmountFromFullTokens(90))
      expect(await noteIssuerConnection.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(100))
      expect(await noteIssuerConnection.notesIssued(issuer1.address)).to.equal(getAmountFromFullTokens(100))
      expect(await noteIssuerConnection.totalCollateral()).to.equal(getAmountFromFullTokens(90))

      await noteIssuerConnection.unwind(getAmountFromFullTokens(50), issuer1.address)

      expect(await daiIssuerConnection.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(955))
      expect(await daiIssuerConnection.balanceOf(note.address)).to.equal(getAmountFromFullTokens(45))
      expect(await noteIssuerConnection.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(50))
      expect(await noteIssuerConnection.notesIssued(issuer1.address)).to.equal(getAmountFromFullTokens(50))
      expect(await noteIssuerConnection.totalCollateral()).to.equal(getAmountFromFullTokens(45))

      await noteIssuerConnection.unwind(getAmountFromFullTokens(50), issuer1.address)

      expect(await daiIssuerConnection.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(1000))
      expect(await daiIssuerConnection.balanceOf(note.address)).to.equal(getAmountFromFullTokens(0))
      expect(await noteIssuerConnection.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(0))
      expect(await noteIssuerConnection.notesIssued(issuer1.address)).to.equal(getAmountFromFullTokens(0))
      expect(await noteIssuerConnection.totalCollateral()).to.equal(getAmountFromFullTokens(0))
    });

    it('should issue and unwind note tokens correctly with several writers', async function () {
      let noteIssuerConnection1 = note.connect(issuer1)
      let daiIssuerConnection1 = daiToken.connect(issuer1)

      let noteIssuerConnection2 = note.connect(issuer2)
      let daiIssuerConnection2 = daiToken.connect(issuer2)

      await daiIssuerConnection1.approve(note.address, getAmountFromFullTokens(90))
      await daiIssuerConnection2.approve(note.address, getAmountFromFullTokens(180))

      await noteIssuerConnection1.issue(getAmountFromFullTokens(100), issuer1.address)
      await noteIssuerConnection2.issue(getAmountFromFullTokens(200), issuer2.address)

      expect(await daiIssuerConnection1.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(910))
      expect(await daiIssuerConnection2.balanceOf(issuer2.address)).to.equal(getAmountFromFullTokens(820))

      expect(await daiIssuerConnection1.balanceOf(note.address)).to.equal(getAmountFromFullTokens(270))

      expect(await noteIssuerConnection1.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(100))
      expect(await noteIssuerConnection2.balanceOf(issuer2.address)).to.equal(getAmountFromFullTokens(200))

      await noteIssuerConnection2.transfer(issuer1.address, getAmountFromFullTokens(100))

      expect(await noteIssuerConnection1.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(200))
      expect(await noteIssuerConnection2.balanceOf(issuer2.address)).to.equal(getAmountFromFullTokens(100))

      expect(await noteIssuerConnection1.notesIssued(issuer1.address)).to.equal(getAmountFromFullTokens(100))
      expect(await noteIssuerConnection2.notesIssued(issuer2.address)).to.equal(getAmountFromFullTokens(200))

      expect(await noteIssuerConnection1.totalCollateral()).to.equal(getAmountFromFullTokens(270))

      await noteIssuerConnection1.unwind(getAmountFromFullTokens(100), issuer1.address)

      expect(await daiIssuerConnection1.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(1000))
      expect(await daiIssuerConnection1.balanceOf(note.address)).to.equal(getAmountFromFullTokens(180))

      expect(await noteIssuerConnection1.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(100))
      expect(await noteIssuerConnection1.notesIssued(issuer1.address)).to.equal(getAmountFromFullTokens(0))
      expect(await noteIssuerConnection1.notesIssued(issuer2.address)).to.equal(getAmountFromFullTokens(200))

      expect(await noteIssuerConnection1.totalCollateral()).to.equal(getAmountFromFullTokens(180))
    });

    it('should not settle before expiry', async function () {
      let noteIssuerConnection1 = note.connect(issuer1)
      let daiIssuerConnection1 = daiToken.connect(issuer1)

      await daiIssuerConnection1.approve(note.address, getAmountFromFullTokens(90))
      await noteIssuerConnection1.issue(getAmountFromFullTokens(100), issuer1.address)

      expect(noteIssuerConnection1.settle(issuer1.address)).to.be.reverted

    });

    it('should settle correctly after expiry', async function () {
      let noteIssuerConnection1 = note.connect(issuer1)
      let daiIssuerConnection1 = daiToken.connect(issuer1)

      let noteIssuerConnection2 = note.connect(issuer2)
      let daiIssuerConnection2 = daiToken.connect(issuer2)

      await daiIssuerConnection1.approve(note.address, getAmountFromFullTokens(90))
      await daiIssuerConnection2.approve(note.address, getAmountFromFullTokens(180))

      await noteIssuerConnection1.issue(getAmountFromFullTokens(100), issuer1.address)
      await noteIssuerConnection2.issue(getAmountFromFullTokens(200), issuer2.address)

      await advanceTime(30);

      await noteIssuerConnection1.settle(issuer1.address)
      await noteIssuerConnection2.settle(issuer2.address)

      expect(await daiIssuerConnection1.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(1000))
      expect(await daiIssuerConnection2.balanceOf(issuer2.address)).to.equal(getAmountFromFullTokens(1000))

      expect(await noteIssuerConnection1.notesIssued(issuer1.address)).to.equal(getAmountFromFullTokens(0))
      expect(await noteIssuerConnection1.notesIssued(issuer2.address)).to.equal(getAmountFromFullTokens(0))
      expect(await noteIssuerConnection1.totalCollateral()).to.equal(0)

    });
});

describe("AuctionSupply", function() {
  beforeEach(async () => {
        [deployer, issuer1, issuer2, buyer1, buyer2] = await hre.ethers.getSigners();

        let noteDeployerFactory = await hre.ethers.getContractFactory("NoteDeployer");
        noteDeployer = await noteDeployerFactory.deploy()

        let auctionDeployerFactory = await hre.ethers.getContractFactory("AuctionDeployer");
        auctionDeployer = await auctionDeployerFactory.deploy()

        let erc20Factory = await hre.ethers.getContractFactory("ERC20Mock");

        aegisToken = await erc20Factory.deploy("Aegis Token", "AEGIS");
        daiToken = await erc20Factory.deploy("Dai", "DAI");
        lpsToken = await erc20Factory.deploy("Pool LPS", "LPS");

        let athenaFactory = await hre.ethers.getContractFactory("AthenaMock");
        athena = await athenaFactory.deploy(aegisToken.address);

        await aegisToken.mint(deployer.address, getAmountFromFullTokens(50));
        await daiToken.mint(issuer1.address, getAmountFromFullTokens(1000));
        await daiToken.mint(issuer2.address, getAmountFromFullTokens(1000));
        await daiToken.mint(buyer1.address, getAmountFromFullTokens(1000));
        await daiToken.mint(buyer2.address, getAmountFromFullTokens(1000));

        let factoryFactory = await hre.ethers.getContractFactory("NoteFactory");
        factory = await factoryFactory.deploy(aegisToken.address, athena.address, noteDeployer.address, auctionDeployer.address);

        let factoryDeployerConnection = factory.connect(deployer)
        let aegisDeployerConnection = aegisToken.connect(deployer)

        let noteCreationBond = await athena.getPCNCreationBond()
        await aegisDeployerConnection.approve(factory.address, noteCreationBond)

        let expiryTime = await getChainTimestamp() + 60*60*24*30

        addresses = await factoryDeployerConnection.callStatic.deployNote(daiToken.address, lpsToken.address, 900, 2500, expiryTime)
        await factoryDeployerConnection.deployNote(daiToken.address, lpsToken.address, 900, 2500, expiryTime)

        currentTimestamp = await getChainTimestamp()
        auctionStart = currentTimestamp + 60*60*12
        auctionEnd = currentTimestamp + 60*60*24

        let noteFactory = await hre.ethers.getContractFactory("CoverageNote")
        note = noteFactory.attach(addresses.noteAddress)

        let auctionFactory = await hre.ethers.getContractFactory("GenesisAuction")
        auction = auctionFactory.attach(addresses.auctionAddress)

    });

    it('should add asks to the ask list correctly', async function () {
      let noteIssuerConnection1 = note.connect(issuer1)
      let daiIssuerConnection1 = daiToken.connect(issuer1)
      let auctionIssuerConnection1 = auction.connect(issuer1)

      let noteIssuerConnection2 = note.connect(issuer2)
      let daiIssuerConnection2 = daiToken.connect(issuer2)
      let auctionIssuerConnection2 = auction.connect(issuer2)

      await daiIssuerConnection1.approve(note.address, getAmountFromFullTokens(90))
      await daiIssuerConnection2.approve(note.address, getAmountFromFullTokens(180))

      await noteIssuerConnection1.issue(getAmountFromFullTokens(100), issuer1.address)
      await noteIssuerConnection2.issue(getAmountFromFullTokens(200), issuer2.address)

      await noteIssuerConnection1.approve(auction.address, getAmountFromFullTokens(100))
      await noteIssuerConnection2.approve(auction.address, getAmountFromFullTokens(200))

      let askList = await auctionIssuerConnection1.getAskList()

      let headAskId = askList[0]
      let tailAskId = askList[1]

      let newAskId1 = await auctionIssuerConnection1.callStatic.insertAsk(getAmountFromFullTokens(100), getAmountFromFullTokens(0.5), headAskId, tailAskId);
      await auctionIssuerConnection1.insertAsk(getAmountFromFullTokens(100), getAmountFromFullTokens(0.5), headAskId, tailAskId);

      expect(auctionIssuerConnection2.insertAsk(getAmountFromFullTokens(200), getAmountFromFullTokens(0.1), headAskId, tailAskId)).to.be.reverted;

      let newAskId2 = await auctionIssuerConnection2.callStatic.insertAsk(getAmountFromFullTokens(200), getAmountFromFullTokens(0.1), headAskId, newAskId1);
      await auctionIssuerConnection2.insertAsk(getAmountFromFullTokens(200), getAmountFromFullTokens(0.1), headAskId, newAskId1);

      expect(await auctionIssuerConnection1.totalNotesOffered()).to.equal(getAmountFromFullTokens(300));
      expect((await auctionIssuerConnection1.getAskList()).length).to.equal(4);

      let ask1 = await auctionIssuerConnection1.getAsk(newAskId1);
      let ask2 = await auctionIssuerConnection2.getAsk(newAskId2);

      expect(ask2.nextId).to.equal(ask1.id)
      expect(ask1.prevId).to.equal(ask2.id)
    });

    // TODO: This test fails from time to time
    // It seems that hardhat may add and extra second when advancing time, which makes the price slightly lower than 0.9 DAI
    it('should compute the price correctly', async function () {
      let noteIssuerConnection1 = note.connect(issuer1)
      let daiIssuerConnection1 = daiToken.connect(issuer1)
      let auctionIssuerConnection1 = auction.connect(issuer1)

      let noteIssuerConnection2 = note.connect(issuer2)
      let daiIssuerConnection2 = daiToken.connect(issuer2)
      let auctionIssuerConnection2 = auction.connect(issuer2)

      await daiIssuerConnection1.approve(note.address, getAmountFromFullTokens(90))
      await daiIssuerConnection2.approve(note.address, getAmountFromFullTokens(180))

      await noteIssuerConnection1.issue(getAmountFromFullTokens(100), issuer1.address)
      await noteIssuerConnection2.issue(getAmountFromFullTokens(200), issuer2.address)

      await noteIssuerConnection1.approve(auction.address, getAmountFromFullTokens(100))
      await noteIssuerConnection2.approve(auction.address, getAmountFromFullTokens(200))

      let askList = await auctionIssuerConnection1.getAskList()

      let headAskId = askList[0]
      let tailAskId = askList[1]

      let newAskId1 = await auctionIssuerConnection1.callStatic.insertAsk(getAmountFromFullTokens(100), getAmountFromFullTokens(0.5), headAskId, tailAskId);
      await auctionIssuerConnection1.insertAsk(getAmountFromFullTokens(100), getAmountFromFullTokens(0.5), headAskId, tailAskId);

      expect(auctionIssuerConnection2.insertAsk(getAmountFromFullTokens(200), getAmountFromFullTokens(0.1), headAskId, tailAskId)).to.be.reverted;

      let newAskId2 = await auctionIssuerConnection2.callStatic.insertAsk(getAmountFromFullTokens(200), getAmountFromFullTokens(0.1), headAskId, newAskId1);
      await auctionIssuerConnection2.insertAsk(getAmountFromFullTokens(200), getAmountFromFullTokens(0.1), headAskId, newAskId1);

      let currentTimestamp = await getChainTimestamp()

      advanceTimeSeconds(auctionStart - currentTimestamp)

      expect(await auctionIssuerConnection1.getCurrentPrice()).to.equal(getAmountFromFullTokens(0.9))
      expect(await auctionIssuerConnection1.getStage()).to.equal(1)

      advanceTimeSeconds(6*60*60)

      expect(await auctionIssuerConnection1.getCurrentPrice()).to.equal(getAmountFromFullTokens(0.5))

      advanceTimeSeconds(6*60*60)

      expect(await auctionIssuerConnection1.getCurrentPrice()).to.equal(getAmountFromFullTokens(0.1))
    });

    it('should not allow to bid before auction start', async function () {
      let auctionBuyerConnection1 = auction.connect(buyer1)
      expect(auctionBuyerConnection1.bid(getAmountFromFullTokens(100))).to.be.reverted;
    });
});


describe("AuctionTrading", function() {
  beforeEach(async () => {
        [deployer, issuer1, issuer2, buyer1, buyer2] = await hre.ethers.getSigners();

        let noteDeployerFactory = await hre.ethers.getContractFactory("NoteDeployer");
        noteDeployer = await noteDeployerFactory.deploy()

        let auctionDeployerFactory = await hre.ethers.getContractFactory("AuctionDeployer");
        auctionDeployer = await auctionDeployerFactory.deploy()

        let erc20Factory = await hre.ethers.getContractFactory("ERC20Mock");

        aegisToken = await erc20Factory.deploy("Aegis Token", "AEGIS");
        daiToken = await erc20Factory.deploy("Dai", "DAI");
        lpsToken = await erc20Factory.deploy("Pool LPS", "LPS");

        let athenaFactory = await hre.ethers.getContractFactory("AthenaMock");
        athena = await athenaFactory.deploy(aegisToken.address);

        await aegisToken.mint(deployer.address, getAmountFromFullTokens(50));
        await daiToken.mint(issuer1.address, getAmountFromFullTokens(10000));
        await daiToken.mint(issuer2.address, getAmountFromFullTokens(10000));
        await daiToken.mint(buyer1.address, getAmountFromFullTokens(10000));
        await daiToken.mint(buyer2.address, getAmountFromFullTokens(10000));

        let factoryFactory = await hre.ethers.getContractFactory("NoteFactory");
        factory = await factoryFactory.deploy(aegisToken.address, athena.address, noteDeployer.address, auctionDeployer.address);

        let factoryDeployerConnection = factory.connect(deployer)
        let aegisDeployerConnection = aegisToken.connect(deployer)

        let noteCreationBond = await athena.getPCNCreationBond()
        await aegisDeployerConnection.approve(factory.address, noteCreationBond)

        let expiryTime = await getChainTimestamp() + 60*60*24*30

        addresses = await factoryDeployerConnection.callStatic.deployNote(daiToken.address, lpsToken.address, 900, 2500, expiryTime)
        await factoryDeployerConnection.deployNote(daiToken.address, lpsToken.address, 900, 2500, expiryTime)

        currentTimestamp = await getChainTimestamp()
        auctionStart = currentTimestamp + 60*60*12
        auctionEnd = currentTimestamp + 60*60*24

        let noteFactory = await hre.ethers.getContractFactory("CoverageNote")
        note = noteFactory.attach(addresses.noteAddress)

        let auctionFactory = await hre.ethers.getContractFactory("GenesisAuction")
        auction = auctionFactory.attach(addresses.auctionAddress)

        let noteIssuerConnection1 = note.connect(issuer1)
        let daiIssuerConnection1 = daiToken.connect(issuer1)
        let auctionIssuerConnection1 = auction.connect(issuer1)

        let noteIssuerConnection2 = note.connect(issuer2)
        let daiIssuerConnection2 = daiToken.connect(issuer2)
        let auctionIssuerConnection2 = auction.connect(issuer2)

        await daiIssuerConnection1.approve(note.address, getAmountFromFullTokens(900))
        await daiIssuerConnection2.approve(note.address, getAmountFromFullTokens(1800))

        await noteIssuerConnection1.issue(getAmountFromFullTokens(1000), issuer1.address)
        await noteIssuerConnection2.issue(getAmountFromFullTokens(2000), issuer2.address)

        await noteIssuerConnection1.approve(auction.address, getAmountFromFullTokens(1000))
        await noteIssuerConnection2.approve(auction.address, getAmountFromFullTokens(2000))

        let askList = await auctionIssuerConnection1.getAskList()

        headAskId = askList[0]
        tailAskId = askList[1]

        newAskId1 = await auctionIssuerConnection1.callStatic.insertAsk(getAmountFromFullTokens(100), getAmountFromFullTokens(0.3), headAskId, tailAskId);
        await auctionIssuerConnection1.insertAsk(getAmountFromFullTokens(100), getAmountFromFullTokens(0.3), headAskId, tailAskId);

        newAskId2 = await auctionIssuerConnection2.callStatic.insertAsk(getAmountFromFullTokens(200), getAmountFromFullTokens(0.1), headAskId, newAskId1);
        await auctionIssuerConnection2.insertAsk(getAmountFromFullTokens(200), getAmountFromFullTokens(0.1), headAskId, newAskId1);

        currentTimestamp = await getChainTimestamp()

        await advanceTimeSeconds(auctionStart - currentTimestamp)

    });

    it('should accept bids', async function () {
      let auctionBuyerConnection1 = auction.connect(buyer1)
      let daiBuyerConnection1 = daiToken.connect(buyer1)

      await daiBuyerConnection1.approve(auction.address, getAmountFromFullTokens(90))

      await auctionBuyerConnection1.bid(getAmountFromFullTokens(100));

      let ask = await auctionBuyerConnection1.getAsk(newAskId2);

      expect(ask.qtySold).to.equal(getAmountFromFullTokens(100))
      expect(await auctionBuyerConnection1.totalNotesSold()).to.equal(getAmountFromFullTokens(100))
    });

    it('should accept bids that consume multiple asks', async function () {
      let auctionBuyerConnection1 = auction.connect(buyer1)
      let daiBuyerConnection1 = daiToken.connect(buyer1)

      await advanceTimeSeconds(6*60*60)

      await daiBuyerConnection1.approve(auction.address, getAmountFromFullTokens(125))

      await auctionBuyerConnection1.bid(getAmountFromFullTokens(250));

      let ask2 = await auctionBuyerConnection1.getAsk(newAskId2);
      let ask1 = await auctionBuyerConnection1.getAsk(newAskId1);

      expect(ask2.qtySold).to.equal(getAmountFromFullTokens(200))
      expect(ask1.qtySold).to.equal(getAmountFromFullTokens(50))

      expect(await auctionBuyerConnection1.totalNotesSold()).to.equal(getAmountFromFullTokens(250))
    });

    it('should not allow to clear the auction before end', async function () {
      let auctionBuyerConnection1 = auction.connect(buyer1)
      let daiBuyerConnection1 = daiToken.connect(buyer1)

      await advanceTimeSeconds(6*60*60)

      await daiBuyerConnection1.approve(auction.address, getAmountFromFullTokens(150))

      await auctionBuyerConnection1.bid(getAmountFromFullTokens(300));

      expect(auctionBuyerConnection1.clearAuction()).to.be.reverted;


    });

    it('should correctly resolve when the entire supply is consumed', async function () {
      let auctionBuyerConnection1 = auction.connect(buyer1)
      let daiBuyerConnection1 = daiToken.connect(buyer1)
      let noteBuyerConnection1 = note.connect(buyer1)

      await advanceTimeSeconds(6*60*60)

      await daiBuyerConnection1.approve(auction.address, getAmountFromFullTokens(150))

      await auctionBuyerConnection1.bid(getAmountFromFullTokens(300));

      let ask2 = await auctionBuyerConnection1.getAsk(newAskId2);
      let ask1 = await auctionBuyerConnection1.getAsk(newAskId1);

      expect(ask2.qtySold).to.equal(getAmountFromFullTokens(200))
      expect(ask1.qtySold).to.equal(getAmountFromFullTokens(100))

      expect(await auctionBuyerConnection1.totalNotesSold()).to.equal(getAmountFromFullTokens(300))

      await advanceTimeSeconds(6*60*60)

      await auctionBuyerConnection1.clearAuction()

      expect(await auctionBuyerConnection1.getCurrentPrice()).to.equal(getAmountFromFullTokens(0.3))

      await auctionBuyerConnection1.resolveBid()

      expect(await daiBuyerConnection1.balanceOf(buyer1.address)).to.equal(getAmountFromFullTokens(9910))
      expect(await noteBuyerConnection1.balanceOf(buyer1.address)).to.equal(getAmountFromFullTokens(300))

      let noteIssuerConnection1 = note.connect(issuer1)
      let daiIssuerConnection1 = daiToken.connect(issuer1)
      let auctionIssuerConnection1 = auction.connect(issuer1)

      let noteIssuerConnection2 = note.connect(issuer2)
      let daiIssuerConnection2 = daiToken.connect(issuer2)
      let auctionIssuerConnection2 = auction.connect(issuer2)

      await auctionIssuerConnection1.resolveAsk(newAskId1)

      expect(await daiIssuerConnection1.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(9100 + 30))
      expect(await noteIssuerConnection1.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(900))

      await auctionIssuerConnection2.resolveAsk(newAskId2)

      expect(await daiIssuerConnection2.balanceOf(issuer2.address)).to.equal(getAmountFromFullTokens(8200 + 60))
      expect(await noteIssuerConnection2.balanceOf(issuer2.address)).to.equal(getAmountFromFullTokens(1800))

      expect(await daiIssuerConnection1.balanceOf(auction.address)).to.equal(0)
      expect(await noteIssuerConnection1.balanceOf(auction.address)).to.equal(0)
    });

    it('should correctly resolve when a bidder attempts to buy more than the current supply', async function () {
      let auctionBuyerConnection1 = auction.connect(buyer1)
      let daiBuyerConnection1 = daiToken.connect(buyer1)
      let noteBuyerConnection1 = note.connect(buyer1)

      await advanceTimeSeconds(10*60*60)

      await daiBuyerConnection1.approve(auction.address, getAmountFromFullTokens(150))

      await auctionBuyerConnection1.bid(getAmountFromFullTokens(300));

      let ask2 = await auctionBuyerConnection1.getAsk(newAskId2);
      let ask1 = await auctionBuyerConnection1.getAsk(newAskId1);

      expect(ask2.qtySold).to.equal(getAmountFromFullTokens(200))
      expect(ask1.qtySold).to.equal(getAmountFromFullTokens(0))

      expect(await auctionBuyerConnection1.totalNotesSold()).to.equal(getAmountFromFullTokens(200))

      await advanceTimeSeconds(6*60*60)

      await auctionBuyerConnection1.clearAuction()

      expect(await auctionBuyerConnection1.getCurrentPrice()).to.equal(getAmountFromFullTokens(0.1))

      await auctionBuyerConnection1.resolveBid()

      expect(await daiBuyerConnection1.balanceOf(buyer1.address)).to.equal(getAmountFromFullTokens(9980))
      expect(await noteBuyerConnection1.balanceOf(buyer1.address)).to.equal(getAmountFromFullTokens(200))

      let noteIssuerConnection1 = note.connect(issuer1)
      let daiIssuerConnection1 = daiToken.connect(issuer1)
      let auctionIssuerConnection1 = auction.connect(issuer1)

      let noteIssuerConnection2 = note.connect(issuer2)
      let daiIssuerConnection2 = daiToken.connect(issuer2)
      let auctionIssuerConnection2 = auction.connect(issuer2)

      await auctionIssuerConnection1.resolveAsk(newAskId1)

      expect(await daiIssuerConnection1.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(9100))
      expect(await noteIssuerConnection1.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(1000))

      await auctionIssuerConnection2.resolveAsk(newAskId2)

      expect(await daiIssuerConnection2.balanceOf(issuer2.address)).to.equal(getAmountFromFullTokens(8200 + 20))
      expect(await noteIssuerConnection2.balanceOf(issuer2.address)).to.equal(getAmountFromFullTokens(1800))

      expect(await daiIssuerConnection1.balanceOf(auction.address)).to.equal(0)
      expect(await noteIssuerConnection1.balanceOf(auction.address)).to.equal(0)
    });

    it('should correctly resolve when less than the entire supply is consumed', async function () {
      let auctionBuyerConnection1 = auction.connect(buyer1)
      let daiBuyerConnection1 = daiToken.connect(buyer1)
      let noteBuyerConnection1 = note.connect(buyer1)

      await advanceTimeSeconds(6*60*60)

      await daiBuyerConnection1.approve(auction.address, getAmountFromFullTokens(115))

      await auctionBuyerConnection1.bid(getAmountFromFullTokens(230));

      let ask2 = await auctionBuyerConnection1.getAsk(newAskId2);
      let ask1 = await auctionBuyerConnection1.getAsk(newAskId1);

      expect(ask2.qtySold).to.equal(getAmountFromFullTokens(200))
      expect(ask1.qtySold).to.equal(getAmountFromFullTokens(30))

      expect(await auctionBuyerConnection1.totalNotesSold()).to.equal(getAmountFromFullTokens(230))

      await advanceTimeSeconds(6*60*60)

      await auctionBuyerConnection1.clearAuction()

      expect(await auctionBuyerConnection1.getCurrentPrice()).to.equal(getAmountFromFullTokens(0.3))

      await auctionBuyerConnection1.resolveBid()

      expect(await daiBuyerConnection1.balanceOf(buyer1.address)).to.equal(getAmountFromFullTokens(9931))
      expect(await noteBuyerConnection1.balanceOf(buyer1.address)).to.equal(getAmountFromFullTokens(230))

      let noteIssuerConnection1 = note.connect(issuer1)
      let daiIssuerConnection1 = daiToken.connect(issuer1)
      let auctionIssuerConnection1 = auction.connect(issuer1)

      let noteIssuerConnection2 = note.connect(issuer2)
      let daiIssuerConnection2 = daiToken.connect(issuer2)
      let auctionIssuerConnection2 = auction.connect(issuer2)

      await auctionIssuerConnection1.resolveAsk(newAskId1)

      expect(await daiIssuerConnection1.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(9100 + 9))
      expect(await noteIssuerConnection1.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(970))

      await auctionIssuerConnection2.resolveAsk(newAskId2)

      expect(await daiIssuerConnection2.balanceOf(issuer2.address)).to.equal(getAmountFromFullTokens(8200 + 60))
      expect(await noteIssuerConnection2.balanceOf(issuer2.address)).to.equal(getAmountFromFullTokens(1800))

      expect(await daiIssuerConnection1.balanceOf(auction.address)).to.equal(0)
      expect(await noteIssuerConnection1.balanceOf(auction.address)).to.equal(0)
    });

});

describe("AuctionTradingComplex", function() {
  beforeEach(async () => {
        [deployer, issuer1, issuer2, buyer1, buyer2] = await hre.ethers.getSigners();

        let noteDeployerFactory = await hre.ethers.getContractFactory("NoteDeployer");
        noteDeployer = await noteDeployerFactory.deploy()

        let auctionDeployerFactory = await hre.ethers.getContractFactory("AuctionDeployer");
        auctionDeployer = await auctionDeployerFactory.deploy()

        let erc20Factory = await hre.ethers.getContractFactory("ERC20Mock");

        aegisToken = await erc20Factory.deploy("Aegis Token", "AEGIS");
        daiToken = await erc20Factory.deploy("Dai", "DAI");
        lpsToken = await erc20Factory.deploy("Pool LPS", "LPS");

        let athenaFactory = await hre.ethers.getContractFactory("AthenaMock");
        athena = await athenaFactory.deploy(aegisToken.address);

        await aegisToken.mint(deployer.address, getAmountFromFullTokens(50));
        await daiToken.mint(issuer1.address, getAmountFromFullTokens(10000));
        await daiToken.mint(issuer2.address, getAmountFromFullTokens(10000));
        await daiToken.mint(buyer1.address, getAmountFromFullTokens(10000));
        await daiToken.mint(buyer2.address, getAmountFromFullTokens(10000));

        let factoryFactory = await hre.ethers.getContractFactory("NoteFactory");
        factory = await factoryFactory.deploy(aegisToken.address, athena.address, noteDeployer.address, auctionDeployer.address);

        let factoryDeployerConnection = factory.connect(deployer)
        let aegisDeployerConnection = aegisToken.connect(deployer)

        let noteCreationBond = await athena.getPCNCreationBond()
        await aegisDeployerConnection.approve(factory.address, noteCreationBond)

        let expiryTime = await getChainTimestamp() + 60*60*24*30

        addresses = await factoryDeployerConnection.callStatic.deployNote(daiToken.address, lpsToken.address, 900, 2500, expiryTime)
        await factoryDeployerConnection.deployNote(daiToken.address, lpsToken.address, 900, 2500, expiryTime)

        currentTimestamp = await getChainTimestamp()
        auctionStart = currentTimestamp + 60*60*12
        auctionEnd = currentTimestamp + 60*60*24

        let noteFactory = await hre.ethers.getContractFactory("CoverageNote")
        note = noteFactory.attach(addresses.noteAddress)

        let auctionFactory = await hre.ethers.getContractFactory("GenesisAuction")
        auction = auctionFactory.attach(addresses.auctionAddress)

        let noteIssuerConnection1 = note.connect(issuer1)
        let daiIssuerConnection1 = daiToken.connect(issuer1)
        let auctionIssuerConnection1 = auction.connect(issuer1)

        let noteIssuerConnection2 = note.connect(issuer2)
        let daiIssuerConnection2 = daiToken.connect(issuer2)
        let auctionIssuerConnection2 = auction.connect(issuer2)

        await daiIssuerConnection1.approve(note.address, getAmountFromFullTokens(900))
        await daiIssuerConnection2.approve(note.address, getAmountFromFullTokens(1800))

        await noteIssuerConnection1.issue(getAmountFromFullTokens(1000), issuer1.address)
        await noteIssuerConnection2.issue(getAmountFromFullTokens(2000), issuer2.address)

        await noteIssuerConnection1.approve(auction.address, getAmountFromFullTokens(1000))
        await noteIssuerConnection2.approve(auction.address, getAmountFromFullTokens(2000))

        let askList = await auctionIssuerConnection1.getAskList()

        headAskId = askList[0]
        tailAskId = askList[1]

        newAskId1 = await auctionIssuerConnection1.callStatic.insertAsk(getAmountFromFullTokens(100), getAmountFromFullTokens(0.3), headAskId, tailAskId);
        await auctionIssuerConnection1.insertAsk(getAmountFromFullTokens(100), getAmountFromFullTokens(0.3), headAskId, tailAskId);

        newAskId2 = await auctionIssuerConnection2.callStatic.insertAsk(getAmountFromFullTokens(200), getAmountFromFullTokens(0.1), headAskId, newAskId1);
        await auctionIssuerConnection2.insertAsk(getAmountFromFullTokens(200), getAmountFromFullTokens(0.1), headAskId, newAskId1);

        newAskId3 = await auctionIssuerConnection1.callStatic.insertAsk(getAmountFromFullTokens(300), getAmountFromFullTokens(0.4), newAskId1, tailAskId);
        await auctionIssuerConnection1.insertAsk(getAmountFromFullTokens(300), getAmountFromFullTokens(0.4), newAskId1, tailAskId);

        newAskId4 = await auctionIssuerConnection2.callStatic.insertAsk(getAmountFromFullTokens(500), getAmountFromFullTokens(0.35), newAskId1, newAskId3);
        await auctionIssuerConnection2.insertAsk(getAmountFromFullTokens(500), getAmountFromFullTokens(0.35), newAskId1, newAskId3);

        currentTimestamp = await getChainTimestamp()

        await advanceTimeSeconds(auctionStart - currentTimestamp)

    });

    it('should correctly resolve in a complex scenario with multiple buyers and sellers', async function () {
      let auctionBuyerConnection1 = auction.connect(buyer1)
      let daiBuyerConnection1 = daiToken.connect(buyer1)
      let noteBuyerConnection1 = note.connect(buyer1)

      let auctionBuyerConnection2 = auction.connect(buyer2)
      let daiBuyerConnection2 = daiToken.connect(buyer2)
      let noteBuyerConnection2 = note.connect(buyer2)

      let noteIssuerConnection1 = note.connect(issuer1)
      let daiIssuerConnection1 = daiToken.connect(issuer1)
      let auctionIssuerConnection1 = auction.connect(issuer1)

      let noteIssuerConnection2 = note.connect(issuer2)
      let daiIssuerConnection2 = daiToken.connect(issuer2)
      let auctionIssuerConnection2 = auction.connect(issuer2)

      await advanceTimeSeconds(3*60*60)

      await daiBuyerConnection1.approve(auction.address, getAmountFromFullTokens(210))

      await auctionBuyerConnection1.bid(getAmountFromFullTokens(300));

      await advanceTimeSeconds(4*60*60)

      let currentPrice = await auctionBuyerConnection2.getCurrentPrice()

      await daiBuyerConnection2.approve(auction.address, currentPrice.mul(200))
      await auctionBuyerConnection2.bid(getAmountFromFullTokens(200))

      let ask1 = await auctionBuyerConnection1.getAsk(newAskId1);
      let ask2 = await auctionBuyerConnection1.getAsk(newAskId2);
      let ask3 = await auctionBuyerConnection1.getAsk(newAskId3);
      let ask4 = await auctionBuyerConnection1.getAsk(newAskId4);


      expect(ask1.qtySold).to.equal(getAmountFromFullTokens(100))
      expect(ask2.qtySold).to.equal(getAmountFromFullTokens(200))
      expect(ask3.qtySold).to.equal(getAmountFromFullTokens(0))
      expect(ask4.qtySold).to.equal(getAmountFromFullTokens(200))

      expect(await auctionBuyerConnection1.totalNotesSold()).to.equal(getAmountFromFullTokens(500))

      await advanceTimeSeconds(6*60*60)

      await auctionBuyerConnection1.clearAuction()

      expect(await auctionBuyerConnection1.getCurrentPrice()).to.equal(getAmountFromFullTokens(0.35))

      await auctionBuyerConnection1.resolveBid()

      expect(await daiBuyerConnection1.balanceOf(buyer1.address)).to.equal(getAmountFromFullTokens(9895))
      expect(await noteBuyerConnection1.balanceOf(buyer1.address)).to.equal(getAmountFromFullTokens(300))

      await auctionBuyerConnection2.resolveBid()

      expect(await daiBuyerConnection2.balanceOf(buyer2.address)).to.equal(getAmountFromFullTokens(9930))
      expect(await noteBuyerConnection2.balanceOf(buyer2.address)).to.equal(getAmountFromFullTokens(200))

      await auctionIssuerConnection1.resolveAsk(newAskId1)

      expect(await daiIssuerConnection1.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(9100 + 35))
      expect(await noteIssuerConnection1.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(600))

      await auctionIssuerConnection2.resolveAsk(newAskId2)

      expect(await daiIssuerConnection2.balanceOf(issuer2.address)).to.equal(getAmountFromFullTokens(8200 + 70))
      expect(await noteIssuerConnection2.balanceOf(issuer2.address)).to.equal(getAmountFromFullTokens(1300))

      await auctionIssuerConnection1.resolveAsk(newAskId3)

      expect(await daiIssuerConnection1.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(9100 + 35))
      expect(await noteIssuerConnection1.balanceOf(issuer1.address)).to.equal(getAmountFromFullTokens(900))

      await auctionIssuerConnection2.resolveAsk(newAskId4)

      expect(await daiIssuerConnection2.balanceOf(issuer2.address)).to.equal(getAmountFromFullTokens(8200 + 70 + 70))
      expect(await noteIssuerConnection2.balanceOf(issuer2.address)).to.equal(getAmountFromFullTokens(1600))

      expect(await daiIssuerConnection1.balanceOf(auction.address)).to.equal(0)
      expect(await noteIssuerConnection1.balanceOf(auction.address)).to.equal(0)
    });

});
