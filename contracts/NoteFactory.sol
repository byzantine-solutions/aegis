pragma solidity 0.7.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";

interface IAthena {
  function getPCNCreationBond() external view returns (uint bond);
  function burnAEGIS(uint amount) external;
}

interface IAuctionDeployer {
  function deployAuction(
    address noteToken,
    address premiumToken,
    uint startTimestamp,
    uint endTimestamp,
    uint maxPrice
  ) external returns (address auction);
}

interface INoteDeployer {
  function deployNote(
    string calldata noteName,
    string calldata noteSymbol,
    address collateral,
    address coveredPoolLPS,
    uint coverageFactor,
    uint maxLeverage,
    uint expiryTimestamp
  ) external returns (address note);
}

contract NoteFactory {
  using SafeMath for uint;

  address public AEGIS;
  address public athena;
  address public noteDeployer;
  address public auctionDeployer;

  mapping (address => bool) private _isNote;
  mapping (address => bool) private _isAuction;
  mapping (bytes32 => address) private _paramsToNote;
  mapping (address => address) private _noteToAuction;

  constructor(address _AEGIS, address _athena, address _noteDeployer, address _auctionDeployer) {
    require(_AEGIS != address(0), "NoteFactory: AEGIS cannot be zero address");
    require(_athena != address(0), "NoteFactory: Athena cannot be zero address");
    require(_noteDeployer != address(0), "NoteFactory: Note deployer cannot be zero address");
    require(_auctionDeployer != address(0), "NoteFactory: Auction deployer cannot be zero address");
    AEGIS = _AEGIS;
    athena = _athena;
    noteDeployer = _noteDeployer;
    auctionDeployer = _auctionDeployer;
  }

  function deployNote(
    address collateral,
    address coveredPoolLPS,
    uint coverageFactor,
    uint maxLeverage,
    uint expiryTimestamp
  ) external returns (address noteAddress, address auctionAddress) {
    uint bondToPay = IAthena(athena).getPCNCreationBond();

    require(IERC20(AEGIS).transferFrom(msg.sender, address(this), bondToPay), "NoteFactory: Could not pull note creation bond");

    IERC20(AEGIS).approve(athena, bondToPay);
    IAthena(athena).burnAEGIS(bondToPay);

    (string memory noteName, string memory noteSymbol) = _getNoteNameAndTicker(collateral, coveredPoolLPS, coverageFactor, maxLeverage, expiryTimestamp);
    
    address note = INoteDeployer(noteDeployer).deployNote(
      noteName,
      noteSymbol,
      collateral,
      coveredPoolLPS,
      coverageFactor,
      maxLeverage,
      expiryTimestamp
    );

    uint maxPrice = coverageFactor.mul(10**18).div(10**3);

    address auction = IAuctionDeployer(auctionDeployer).deployAuction(
      note,
      collateral,
      block.timestamp + 60*60*12,
      block.timestamp + 60*60*24,
      maxPrice
    );

    bytes32 noteHash = _getNoteParamsHash(collateral, coveredPoolLPS, coverageFactor, maxLeverage, expiryTimestamp);

    _isNote[note] = true;
    _isAuction[auction] = true;
    _paramsToNote[noteHash] = note;
    _noteToAuction[note] = auction;

    return (note, auction);
  }

  function getNoteFromParams(
    address collateral,
    address coveredPoolLPS,
    uint coverageFactor,
    uint maxLeverage,
    uint expiryTimestamp
  ) external view returns (address note) {
    bytes32 noteHash = _getNoteParamsHash(collateral, coveredPoolLPS, coverageFactor, maxLeverage, expiryTimestamp);
    return _paramsToNote[noteHash];
  }

  function getAuctionFromNote(address note) external view returns (address auction) {
    return _noteToAuction[note];
  }

  function isNote(address note) external view returns (bool) {
    return _isNote[note];
  }

  function isAuction(address auction) external view returns (bool) {
    return _isAuction[auction];
  }

  // TODO: This is a stub
  function _getNoteNameAndTicker(
    address collateral,
    address coveredPoolLPS,
    uint coverageFactor,
    uint maxLeverage,
    uint expiryTimestamp
  ) internal view returns (string memory name, string memory symbol) {
    return ("Coverage Note", "PCN");
  }

  function _getNoteParamsHash(
    address collateral,
    address coveredPoolLPS,
    uint coverageFactor,
    uint maxLeverage,
    uint expiryTimestamp
  ) internal view returns (bytes32 hash) {
    return keccak256(abi.encode(collateral, coveredPoolLPS, coverageFactor, maxLeverage, expiryTimestamp));
  }

}
