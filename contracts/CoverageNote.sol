//SPDX-License-Identifier: Unlicense
pragma solidity 0.7.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";

contract CoverageNote is ERC20 {
  using SafeMath for uint256;

  uint private DENOMINATOR = 10**3;

  mapping (address => uint) public notesIssued;
  uint public totalNotesIssued;

  address public collateralToken;
  uint public totalCollateral;

  address public coveredPoolLPS;

  uint public coverageFactor;
  uint public maxLeverage;
  uint public expiryTimestamp;

  constructor(
    string memory _name,
    string memory _symbol,
    address _collateralToken,
    address _coveredPoolLPS,
    uint _coverageFactor,
    uint _maxLeverage,
    uint _expiryTimestamp
  )
  ERC20(_name, _symbol)
  {
    require(_collateralToken != address(0), "CoverageNote: The collateral cannot be zero address");
    require(_coveredPoolLPS != address(0), "CoverageNote: The covered LPS cannot be zero address");
    require((_coverageFactor <= 10**3) && (_coverageFactor > 0), "CoverageNote: The coverage factor must be more than 0 and less or equal to 1");
    require(_maxLeverage >= 10**3, "CoverageNote: Max leverage cannot be less than 1");
    require(_expiryTimestamp > block.timestamp, "CoverageNote: The expiry cannot be in the past");

    collateralToken = _collateralToken;
    coveredPoolLPS = _coveredPoolLPS;
    coverageFactor = _coverageFactor;
    maxLeverage = _maxLeverage;
    expiryTimestamp = _expiryTimestamp;
  }

  function issue(uint amount, address receiver) external {
    require(block.timestamp < expiryTimestamp, "CoverageNote: Cannot issue an expired note");
    require(amount > 0, "CoverageNote: Cannot issue zero notes");
    require(receiver != address(0), "CoverageNote: Cannot issue to zero account");

    uint amountToDeposit = amount.mul(coverageFactor).div(DENOMINATOR);

    _deposit(amountToDeposit, msg.sender, receiver);
    _mint(receiver, amount);

    notesIssued[receiver] = notesIssued[receiver].add(amount);
    totalNotesIssued = totalNotesIssued.add(amount);

  }

  function unwind(uint amount, address receiver) external {
    require(amount > 0, "CoverageNote: Cannot unwind zero notes");
    require(receiver != address(0), "CoverageNote: Cannot unwind to zero account");

    uint amountToWithdraw = amount.mul(coverageFactor).div(DENOMINATOR);

    _withdraw(amountToWithdraw, msg.sender, receiver);
    _burn(msg.sender, amount);

    notesIssued[msg.sender] = notesIssued[msg.sender].sub(amount);
    totalNotesIssued = totalNotesIssued.sub(amount);
  }

  function settle(address receiver) external {
    require(block.timestamp > expiryTimestamp, "CoverageNote: Cannot settle before note expiry");
    require(receiver != address(0), "CoverageNote: Cannot settle to zero account");

    uint notesToSettle = notesIssued[msg.sender];
    uint amountToWithdraw = notesToSettle.mul(totalCollateral).div(totalNotesIssued);

    _withdraw(amountToWithdraw, msg.sender, receiver);

    notesIssued[msg.sender] = 0;
    totalNotesIssued = totalNotesIssued.sub(notesToSettle);
  }

  // NB: When implementing, remember that executed shares should be sent to this address instead of burned
  //function execute(uint amount, uint collateralIn) external onlyInsurer {};

  function _deposit(uint amount, address sender, address receiver) internal {

    uint256 amountBefore = IERC20(collateralToken).balanceOf(address(this));
    require(IERC20(collateralToken).transferFrom(sender, address(this), amount), "CoverageNote: Transfer from deposit sender failed");
    require(IERC20(collateralToken).balanceOf(address(this)) == amountBefore.add(amount), "CoverageNote: Incorrect number of pulled tokens on transfer from sender");
    totalCollateral = totalCollateral.add(amount);
  }

  function _withdraw(uint amount, address sender, address receiver) internal {

    require(amount > 0, "CoverageNote: Cannot withdraw zero collateral");

    uint256 amountBefore = IERC20(collateralToken).balanceOf(address(this));
    require(IERC20(collateralToken).transfer(receiver, amount), "CoverageNote: Transfer to receiver failed");
    require(IERC20(collateralToken).balanceOf(address(this)) == amountBefore.sub(amount), "CoverageNote: Incorrect number ot collateral token withdrawn");

    totalCollateral = totalCollateral.sub(amount);
  }

}
