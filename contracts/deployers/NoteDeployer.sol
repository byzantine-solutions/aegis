pragma solidity 0.7.0;

import '../CoverageNote.sol';

contract NoteDeployer {

  function deployNote(
    string calldata noteName,
    string calldata noteSymbol,
    address collateral,
    address coveredPoolLPS,
    uint coverageFactor,
    uint maxLeverage,
    uint expiryTimestamp
  ) external returns (address noteAddress) {
    CoverageNote note = new CoverageNote(noteName, noteSymbol, collateral, coveredPoolLPS, coverageFactor, maxLeverage, expiryTimestamp);
    return address(note);
  }

}
