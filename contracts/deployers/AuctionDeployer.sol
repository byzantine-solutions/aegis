pragma solidity 0.7.0;

import '../GenesisAuction.sol';

contract AuctionDeployer {

  function deployAuction(
    address noteToken,
    address premiumToken,
    uint startTimestamp,
    uint endTimestamp,
    uint maxPrice
  ) external returns (address auctionAddress) {
    GenesisAuction auction = new GenesisAuction(noteToken, premiumToken, startTimestamp, endTimestamp, maxPrice);
    return address(auction);
  }

}
