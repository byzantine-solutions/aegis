pragma solidity 0.7.0;


import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";

contract GenesisAuction {
  using SafeMath for uint;

  address public noteToken;
  uint public totalNotesOffered;
  uint public totalNotesSold;

  address public premiumToken;
  mapping (address => uint) private premiumOffered;
  mapping (address => uint) private notesPurchased;
  uint private maxPrice;

  uint public startTimestamp;
  uint public endTimestamp;

  struct Ask {
    bytes32 id;
    bytes32 prevId;
    bytes32 nextId;
    uint qtyOffered;
    uint qtySold;
    uint minPrice;
    address seller;
  }

  uint private minAskPrice;
  // TODO: Consider if this array is necessary;
  bytes32[] private askIdList;
  bytes32 private currentAsk;

  mapping (bytes32 => Ask) private asks;

  bool private cleared;
  uint private clearingPrice;

  modifier onlyStage(uint stage) {
    require(_getStage() == stage, "GenesisAuction: This action is prohibited at current stage");
    _;
  }

  constructor(
    address _noteToken,
    address _premiumToken,
    uint _startTimestamp,
    uint _endTimestamp,
    uint _maxPrice
  ) {
    require(_noteToken != address(0), "GenesisAuction: Note token cannot be zero account");
    require(_premiumToken != address(0), "GenesisAuction: Premium token cannot be zero account");
    require(_startTimestamp >= block.timestamp + 60*60*12, "GenesisAuction: Auction should start at least 12 hours after creation");
    require(_endTimestamp - _startTimestamp >= 60*60*12, "GenesisAuction: Auction should last at least 12 hours");
    require(_maxPrice > 0, "GenesisAuction: Max price cannot be zero");

    noteToken = _noteToken;
    premiumToken = _premiumToken;
    startTimestamp = _startTimestamp;
    endTimestamp = _endTimestamp;
    maxPrice = _maxPrice;

    bytes32 headAskId = _getAskId(address(0), 0, 0);
    bytes32 tailAskId = _getAskId(address(0), 0, maxPrice);

    asks[headAskId] = Ask(headAskId, "", tailAskId, 0, 0, 0, address(0));
    asks[tailAskId] = Ask(tailAskId, headAskId, "", 0, 0, maxPrice, address(0));

    askIdList.push(headAskId);
    askIdList.push(tailAskId);

    currentAsk = headAskId;
  }

  function insertAsk(uint qtyOffered, uint minPrice, bytes32 prevAskId, bytes32 nextAskId) external onlyStage(0) returns (bytes32 askId) {
    return _insertAsk(msg.sender, qtyOffered, minPrice, prevAskId, nextAskId);
  }

  function _insertAsk(address account, uint qtyOffered, uint minPrice, bytes32 prevAskId, bytes32 nextAskId) internal returns (bytes32 askId) {
    require(account != address(0), "GenesisAuction: Seller cannot be zero account");
    require(qtyOffered > 0, "GenesisAuction: An ask cannot have zero quantity");
    require(minPrice > 0, "GenesisAuction: An ask cannot have zero price");
    require(minPrice < maxPrice, "GenesisAuction: Ask price cannot exceed maximal auction price");

    require(IERC20(noteToken).transferFrom(account, address(this), qtyOffered), "GenesisAuction: Could not pull note token");

    Ask memory prevAsk = asks[prevAskId];
    Ask memory nextAsk = asks[nextAskId];

    require((prevAsk.id == prevAskId) && (nextAsk.id == nextAskId), "GenesisAuction: At least one of neighbors does not exist");
    require((prevAsk.nextId == nextAsk.id) && (nextAsk.prevId == prevAsk.id), "GenesisAuction: Non-adjacent neighbors chosen when inserting an ask");
    require((minPrice >= prevAsk.minPrice) && (minPrice < nextAsk.minPrice), "GenesisAuction: Inserted ask is not between neighbors");

    bytes32 askId = _getAskId(account, qtyOffered, minPrice);
    asks[askId] = Ask(askId, prevAskId, nextAskId, qtyOffered, 0, minPrice, account);
    askIdList.push(askId);

    asks[prevAskId].nextId = askId;
    asks[nextAskId].prevId = askId;

    if (prevAskId == _getHeadAskId()) {
      minAskPrice = minPrice;
      currentAsk = askId;
    }

    totalNotesOffered = totalNotesOffered.add(qtyOffered);

    return askId;
  }

  function bid(uint qtyPurchased) external onlyStage(1) {
    _bid(msg.sender, qtyPurchased);
  }

  function _bid(address bidder, uint qtyPurchased) internal {
    require(bidder != address(0), "GenesisAuction: bidder cannot be zero account");
    require(qtyPurchased > 0, "GenesisAuction: Cannot purchase zero notes");

    uint unfilledQty = qtyPurchased;
    uint currentPrice = _getPrice();
    bytes32 ask = currentAsk;

    require(asks[ask].minPrice < currentPrice, "GenesisAuction: No more supply available at current price");

    while (unfilledQty > 0) {
      uint askQty = asks[ask].qtyOffered.sub(asks[ask].qtySold);
      if (askQty > unfilledQty) {
        asks[ask].qtySold = asks[ask].qtySold.add(unfilledQty);
        unfilledQty = 0;
      } else {
        asks[ask].qtySold = asks[ask].qtyOffered;
        unfilledQty = unfilledQty.sub(askQty);
        ask = asks[ask].nextId;

        if (asks[ask].minPrice > currentPrice) {
          break;
        }
      }
    }

    uint qtySold = qtyPurchased.sub(unfilledQty);
    totalNotesSold = totalNotesSold.add(qtySold);
    uint premiumToPay = qtyPurchased.sub(unfilledQty).mul(currentPrice).div(10**18);
    require(IERC20(premiumToken).transferFrom(bidder, address(this), premiumToPay), "GenesisAuction: Could not pull premium token");

    notesPurchased[bidder] = qtySold;
    premiumOffered[bidder] = premiumToPay;

    currentAsk = ask;
  }

  function clearAuction() external onlyStage(2) {
    _clearAuction();
    // TODO: Implement AEGIS rewards
    _payRewards(msg.sender, 0);
  }

  function _clearAuction() internal {
    cleared = true;
    bytes32 clearAsk;
    if (asks[currentAsk].qtySold != 0) {
      clearAsk = asks[currentAsk].id;
    } else {
      clearAsk = asks[currentAsk].prevId;
    }

    clearingPrice = asks[clearAsk].minPrice;
  }

  function resolveBid() external onlyStage(3) {
    _resolveBid(msg.sender);
  }

  function _resolveBid(address bidder) internal {
    require(bidder != address(0), "GenesisAuction: bidder cannot be zero account");

    uint purchasedNotes = notesPurchased[bidder];
    uint offeredPremium = premiumOffered[bidder];

    uint premiumToPay = purchasedNotes.mul(clearingPrice).div(10**18);

    require(IERC20(noteToken).transfer(bidder, purchasedNotes), "GenesisAuction: Failed to send note tokens");
    require(IERC20(premiumToken).transfer(bidder, offeredPremium.sub(premiumToPay)), "GenesisAuction: Failed to send premium tokens");

    notesPurchased[bidder] = 0;
    premiumOffered[bidder] = 0;
  }

  function resolveAsk(bytes32 askId) external onlyStage(3) {
    _resolveAsk(askId);
  }

  function _resolveAsk(bytes32 askId) internal {
    require(asks[askId].id == askId, "GenesisAuction: The resolved ask does not exist or is already resolved");

    Ask memory ask = asks[askId];
    if (ask.qtySold == 0) {
      require(IERC20(noteToken).transfer(ask.seller, ask.qtyOffered), "GenesisAuction: Failed to send note tokens");
    } else if (ask.qtySold == ask.qtyOffered) {
      uint premiumToReceive = ask.qtySold.mul(clearingPrice).div(10**18);
      require(IERC20(premiumToken).transfer(ask.seller, premiumToReceive), "GenesisAuction: Failed to send premium tokens");
    } else {
      uint premiumToReceive = ask.qtySold.mul(clearingPrice).div(10**18);
      require(IERC20(premiumToken).transfer(ask.seller, premiumToReceive), "GenesisAuction: Failed to send premium tokens");
      require(IERC20(noteToken).transfer(ask.seller, ask.qtyOffered.sub(ask.qtySold)), "GenesisAuction: Failed to send note tokens");
    }

    asks[askId].id = "";
    // TODO: Implement AEGIS distributor contract and rewards
    _payRewards(ask.seller, 0);

  }

  function _payRewards(address beneficiary, uint256 amount) internal {}

  function getAskId(address seller, uint qtyOffered, uint minPrice) external view returns (bytes32 id) {
    return _getAskId(seller, qtyOffered, minPrice);
  }

  function getAskList() external view returns (bytes32[] memory list) {
    return askIdList;
  }

  function _getAskId(address seller, uint qtyOffered, uint minPrice) internal view returns (bytes32 id) {
    return keccak256(abi.encode(seller, qtyOffered, minPrice));
  }

  function _getHeadAskId() internal view returns (bytes32 id) {
    return _getAskId(address(0), 0, 0);
  }

  function _getTailAskId() internal view returns (bytes32 id) {
    return _getAskId(address(0), 0, maxPrice);
  }

  function getStage() external view returns (uint stage) {
    return _getStage();
  }

  function _getStage() internal view returns (uint stage) {
    if (cleared) {
      return 3;
    } else {
      return block.timestamp < startTimestamp ? 0 : (block.timestamp > endTimestamp ? 2 : 1);
    }
  }

  function getCurrentPrice() external view returns (uint price) {
    return _getStage() == 0 ? maxPrice : (_getStage() == 3 ? clearingPrice : _getPrice());
  }

  // (Pmin * (t - t_s) + Pmax * (t_e - t)) / (t_e - t_s)
  function _getPrice() internal view returns (uint price) {
    return (minAskPrice.mul(block.timestamp.sub(startTimestamp)).add(maxPrice.mul(endTimestamp.sub(block.timestamp)))).div(endTimestamp.sub(startTimestamp));
  }

  function getAsk(bytes32 askId) external view returns (bytes32 id, bytes32 prevId, bytes32 nextId, uint qtyOffered, uint qtySold, uint minPrice, address seller) {
    return _getAsk(askId);
  }

  function _getAsk(bytes32 askId) internal view returns (bytes32 id, bytes32 prevId, bytes32 nextId, uint qtyOffered, uint qtySold, uint minPrice, address seller) {
    Ask memory ask = asks[askId];
    return (ask.id, ask.prevId, ask.nextId, ask.qtyOffered, ask.qtySold, ask.minPrice, ask.seller);
  }
}
