pragma solidity 0.7.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract ERC20Mock is ERC20 {

  constructor(
  string memory name,
  string memory symbol)
  ERC20(name, symbol)
  {
  }

  function mint(address account, uint amount) external {
    _mint(account, amount);
  }

  function burn(address account, uint amount) external {
    _burn(account, amount);
  }
}
