pragma solidity 0.7.0;

interface IAegis {
  function burn(address account, uint amount) external;
}


contract AthenaMock {

  address private aegis;

  constructor(address _aegis) {
    aegis = _aegis;
  }

  function getPCNCreationBond() external view returns (uint) {
    return 50 ether;
  }

  function burnAEGIS(uint amount) external {
    IAegis(aegis).burn(msg.sender, amount);
  }
}
